## Assignment

Write an ETL process that prepares sequential event data for analysis according to an
event grammar. The DAG for said ETL process is as follows:

![event dag](https://i.imgur.com/wHITQ7m.png)

### Details

Source data will come in the entity does things over time paradigm of the
normal event logs. They will contain events that refer to different objects
and entities.

Some examples of datasets like this are as follows:

- [kaggle retailrocket dataset](https://www.kaggle.com/retailrocket/ecommerce-dataset)
- [yoochoose recsys dataset](http://recsys.yoochoose.net/challenge.html)
    
These datasets can be conceptually modeled with the event grammar described
[here](https://snowplowanalytics.com/blog/2013/08/12/towards-universal-event-analytics-building-an-event-grammar/)
and put into practice [here](https://snowplowanalytics.com/blog/2014/07/28/explorations-in-analyzing-web-event-data-in-graph-databases/),
[here](https://snowplowanalytics.com/blog/2014/07/30/loading-snowplow-web-event-data-into-graph-databases-for-pathing-analysis/),
and [here](https://snowplowanalytics.com/blog/2014/07/31/using-graph-databases-to-perform-pathing-analysis-initial-experimentation-with-neo4j/).
These 3 blog post links even show a bit of the ETL process that they used to prepare
the data from their own SQL databases.

#### Stage 1

Stage 1 will depend largely on the client and is where most of the man hours
would go on a per-project basis because it will need to be done differently for
every datasource. So for this assignment, we will suggest loading any
event data you are testing with into a postgresql database that mirrors the
csv format of whatever dataset you are using to test.

At the end of this stage, you should have 3 tables from which you can call

```
SELECT * FROM entities;
SELECT * from actions;
SELECT * from things;

SELECT * FROM entity_alias;
SELECT * from action_alias;
SELECT * from thing_alias;
```

as the first set of stage 2.

##### A note on testing

Although you can complete the entire assignment with just one example data
source, it would be good to test with many different source databases and
formats to see if the event grammar that all of this is based upon is
conceptually robust.

#### Stage 2

Now we need to do the heavy transform work in order to put the data into a
set of schemas that make it easy to export to different formats and destinations
that will then be used for analysis and modeling. *The inserting and updating
of all Things, Actions, and Entities should include updating alias tables.
You will need to come up with a general schema for an alias table.

The alias resolving needs to be done because the data may come from several
sources that use different ids for the same user. None of the test datasets
listed here so far use this.

The last part which is about creating the PREV link simply means adding another
foreign key that contains the event id of the previous event performed by
the subject of the event.


#### Stage 3

At this point, we are in the last-mile of processing and are outputting
files directly for import into either code or pre-built dasboards.

Two notebooks that have some very simple pandas code that does pretty
much the entire process are here:
[here](https://gitlab.com/daredata-open-source/neo4j-path-analysis/tree/master/kaggle-retailrocket)
and [here](https://gitlab.com/daredata-open-source/neo4j-path-analysis/tree/master/recsys)

The events with PREV link should be written out simply with no extra frills.
We can do joins later on if we need to. The rest of the things, actions, and
entities csvs are just for backup purposes. It is also very likely that at this
Stage 3 we will need to add more output types on a per-project basis though
at this point its difficult to say how much.


### Suggestions and pointers

You should use [Airflow](https://airflow.apache.org/) for this entire task.
However, if you know of a better-suited tool, feel free to bring it up!

The documentation for neo4j import command which is what the edges and nodes csv
files are for can be found [here](https://neo4j.com/docs/operations-manual/current/tools/import/)

